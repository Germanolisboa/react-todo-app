import {createStore, applyMiddleware, compose} from "redux";
import rootReducer from "./redux/ducks/index";
import thunkMiddleware from "redux-thunk";

export const getStore = () => {
  const middlewares = [
    thunkMiddleware
  ];
  
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    rootReducer,
    {},
    composeEnhancers(applyMiddleware(...middlewares))
  );
    
  return store;
};
  