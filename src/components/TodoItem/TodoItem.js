import React, {useEffect, useState} from 'react'
import styled from 'styled-components'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import moment from 'moment'
import 'moment/locale/pt-br'
import {useGesture} from 'react-with-gesture'
import {useSpring, animated} from 'react-spring'
import {string, bool, number, func} from 'prop-types';
import {Link} from 'react-router-dom'

moment.updateLocale('pt-br')

const StyledItem = styled.div`
  --x-padding: calc(var(--gutter) * 1px);
  position: relative;
  width: 100%;
  height: 55px;
  padding: 0 var(--x-padding);
  cursor: pointer;
  margin-bottom: 1px;

  .due-date {
    margin-top: .2rem;
    font-size: .7rem;
    font-style: italic;
    margin-left: 5px;

    span {
      margin-left: 5px;
    }
  }

  .text-container {
    position: relative;
    padding: 0 var(--x-padding);
    height: 100%;
    z-index: 2;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    background-color: var(--light-background-color);
  }

  .check {
    --checked-scale: 0;
    height: 22px;
    width: 22px;
    border-radius: 50%;
    border: 1px solid currentColor;
    margin-right: 10px;
    position: relative;

    &--checked {
      --checked-scale: 1;
    }

    ::after {
      content: '';
      position: absolute;
      border-radius: 50%;
      top: 0;
      left: 0;
      height: 20px;
      width: 20px;
      transform: scale(var(--checked-scale));
      background-color: var(--accent-color);
      transition: transform 150ms ease 0s;
    }
  }

  .overflow {
    margin-left: auto;
    position: relative;
    padding: 10px 16px;
    --scale: 0.5;
    --translate: 30px;
    --opacity: 0;

    &--opened {
      --scale: 1;
      --translate: 0;
      --opacity: 1;
    }

    &__items {
      position: absolute;
      top: 0;
      right: 40px;
      display: flex;
      align-items: center;
      height: 100%;
      border-radius: 3px;
      box-shadow: 0 3px 12px -3px rgba(0,0,0.5);
      padding: 0 calc(var(--gutter) * 1px);
      transition: transform 250ms ease 0s, opacity 250ms linear 0s;
      transform: scale(var(--scale)) translateX(var(--translate));
      opacity: var(--opacity);
      transform-origin: 100% 50%;

      &__item {
        white-space: nowrap;
        &:first-child {
          margin-right: calc(var(--gutter) * 1px)
        }
      }
    }
  }

  .mark-done {
    position: absolute;
    top: 0;
    left: var(--x-padding);
    height: 100%;
    width: calc(100% - calc(var(--x-padding) * 2));
    background-color: var(--accent-color);
    border-radius: 3px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    padding-left: 30px;
  }

  ::after {
    content: '';
    position: absolute;
    top: 100%;
    height: 1px;
    left: var(--x-padding);
    width: calc(100% - calc(var(--x-padding) * 2));
    background-color: var(--border-color-1);
  }
`

const TodoItem = ({id, text, dueDate, done, onStatusChange, deleteTodo}) => {
  const [state, setState] = useState({
    isWaiting: false,
    lastDown: false,
  })
  const [overflowVisible, setOverflow] = useState(false)

  const [bind, {delta, down}] = useGesture()
  const {x, color, opacity} = useSpring({
    x: (down && delta[1] < 30 && delta[0] > 0) ? delta[0] >= 80 ? 80 + ((delta[0] - 80) / 5) : delta[0] : 0,
    color: delta[0] >= 80 ? '#ffffff' : '#333333',
    opacity: state.isWaiting ? .3 : 1,
    immediate: () => down
  })

  const isDoneClass = done ? 'check--checked' : ''
  const changeStatus = () => {
    !state.isWaiting &&
      onStatusChange({id, text, dueDate, done: !done})
      setState(state => ({...state, isWaiting: true}))
  }

  const scale = x.interpolate({
    range: [20, 70, 80],
    output: ['scale(0.3)', 'scale(1.1)', 'scale(1.4)'],
    extrapolate: 'clamp'
  })

  useEffect(() => {
    if (state.lastDown && !down && delta[0] > 80) {
      changeStatus()
    }
    setState({lastDown: down, isWaiting: false})
  }, [down, done])

  return (
    <StyledItem {...bind()} className="TodoItem">
      <animated.div
        className="text-container"
        style={{transform: x.interpolate(x => `translateX(${x}px)`)}}
      >
        <animated.div 
          className={`check ${isDoneClass}`}
          onClick={changeStatus}
          style={{opacity}}
        ></animated.div>

        <div>
          <div>
            {text}
          </div>

          {dueDate && 
            <div className="due-date">
              <FontAwesomeIcon icon="calendar-week" />
              <span>{moment(dueDate).format('ddd, DD [de] MMMM')}</span>
            </div>
          }
        </div>

        <div 
          className={`overflow ${overflowVisible && 'overflow--opened'}`}  
          onClick={() => setOverflow(!overflowVisible)}
        >
          <FontAwesomeIcon icon="ellipsis-v" />

          <div className="overflow__items">
            <Link to={`/${id}`}>
            <div className="overflow__items__item">
              <FontAwesomeIcon icon="pen" /> Edit
            </div>
            </Link>
            <div
              className="overflow__items__item"
              onClick={() => deleteTodo(id)}
            >
              <FontAwesomeIcon icon="trash-alt" /> Delete
            </div>
          </div>

        </div>

      </animated.div>

      <div className="mark-done">
        <animated.div style={{transform: scale, color}}>
          <FontAwesomeIcon icon="check"/>
        </animated.div>
      </div>
    </StyledItem>
  )
}

TodoItem.propTypes = {
  text: string,
  dueDate: number,
  isDone: bool,
  onStatusChange: func,
  deleteTodo: func
}

export default TodoItem
