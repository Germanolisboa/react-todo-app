import React from 'react'
import {shallow} from 'enzyme'
import TodoItem from './index'
import {stub} from 'sinon'

const statusChangeStub = stub()

const defaultProps = {
  id: 3,
  text: 'text example',
  dueDate: new Date(),
  done: false,
  onStatusChange: statusChangeStub
}

const getComponent = (props = {}) => (
  <TodoItem {...defaultProps} {...props} />
)

describe('TodoItem component', () => {
  it('should render', () => {
    const wrapper = shallow(getComponent())

    expect(wrapper.find('.TodoItem').length).toEqual(1)
  })

  it('should call onStatusChange when clicking on the circle', () => {
     const wrapper = shallow(getComponent())

     wrapper.find('.check').simulate('click')
     expect(statusChangeStub.calledOnce).toBe.true

     statusChangeStub.resetHistory()
  })
})
