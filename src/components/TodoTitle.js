import React from 'react'
import styled from 'styled-components'
import {SCROLLER_POS_VALUES} from './constants/todo'
import {useSpring, animated} from 'react-spring'
import {useScrollerPosition} from './utils/hooks'

const StyledTitle = styled(animated.div)`
  width: 100%;
  padding: calc(var(--gutter) * 2px) calc(var(--gutter) * 1px);
  position: relative;
  z-index: 2;
  transform-origin: 50% 0;
  align-self: flex-start;

  .title {
    font-size: 3rem;
    text-align: center;
  }

  @media (max-width: 500px) {
    .title {
      text-align: left;
    }
    transform-origin: 0 0;
  }
`

const TodoTitle = ({onClick, scrollerRef}) => {
  const scrollPosition = useScrollerPosition(scrollerRef)
  const {pos} = useSpring({
    pos: scrollPosition.value,
    from: {pos: SCROLLER_POS_VALUES.MAX},
    immediate: true
  })

  const transforms = pos.interpolate({
    range: [SCROLLER_POS_VALUES.MIN, SCROLLER_POS_VALUES.MAX],
    output: ['scale(0.5) translateY(-25px)', 'scale(1) translateY(0)'],
    extrapolate: 'clamp'
  })

  return (
    <StyledTitle 
      onClick={onClick}
      style={{
        transform: transforms
      }}
    >
      <div className="title">TODOs</div>
    </StyledTitle>
  )
}

export default TodoTitle
