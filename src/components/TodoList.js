import React, {useEffect} from 'react'
import styled from 'styled-components'
import TodoItem from './TodoItem'
import {connect} from 'react-redux'
import {fetchTodos, setTodoStatus, deleteTodo} from '../redux/ducks/todos'
import {func, object, number, array} from 'prop-types'
import moment from 'moment'
import {Spring, animated} from 'react-spring/renderprops'
import {Route} from 'react-router-dom'
import EditTodo from './editTodo'

const StyledList = styled.div`
  width: 100%;
  max-width: 700px;
  margin: 0 auto;
  min-height: 100%;
  flex-basis: 50%;
  flex-grow: 1;
  border-radius: 60px 60px 0 0;
  position: relative;
  z-index: 3;
  background-color: var(--light-background-color);
  display: flex;
  flex-direction: column;
  align-items: center;
  color: var(--font-color-2);
  padding-bottom: 100px;
  overflow: hidden;

  .header-text {
    opacity: .8;
    font-size: .9rem;
    text-align: center;
    margin: .5rem 0 .6rem;

    &--italic {
      font-style: italic;
    }
  }
  .todo-list {
    width: 100%;
  }
`

const TodoList = ({
  items,
  fetchTodos,
  setTodoStatus,
  error,
  undoneCount,
  deleteTodo
}) => {
  useEffect(() => {
    fetchTodos && fetchTodos()
  }, [fetchTodos])

  const weekTodosCounter = items.length && items.filter(getTodosCountForTheWeek).length
  return (
    <StyledList>
      <div className="header-text">

        <Spring from={{height: 0}} to={{height: undoneCount ? 'auto' : 0}}>
          {props => (
            <animated.div style={{...props, overflow: 'hidden'}}>
              <div>Você tem {undoneCount} tarefa(s) para para completar</div>
              <div className="header-text--italic">
                {weekTodosCounter ? weekTodosCounter : 'nenhuma'} delas para essa semana!
              </div>
            </animated.div>
          )}
        </Spring>
        
      </div>

      <div className="todo-list">
        {items.map(props => (
          <TodoItem 
            {...props}
            key={props.id}
            onStatusChange={setTodoStatus}
            deleteTodo={deleteTodo}
          />
        ))}
      </div>

      <Route path="/:id" children={({match, ...rest}) => {
          if (match.id === 'new') {
            return <EditTodo match={match} {...rest} />
          } else {
            const editingItem = items.find(item => item.id === match.params.id)
            return (
              <EditTodo 
                match={match} 
                editingItem={editingItem}
                {...rest}
              />
            )
          }
      }}/>
    </StyledList>
  )
}

TodoList.propTypes = {
  items: array,
  fetchTodos: func,
  error: object,
  undoneCount: number,
  setTodoStatus: func,
  deleteTodo: func
}

const now = moment()
const nextWeekStart = now.startOf('week').add(1, 'week')
const getTodosCountForTheWeek = item => {
  if (item.dueDate) {
    return moment(item.dueDate).isBetween(now, nextWeekStart)
  }
}

const mapStateToProps = ({
  todos: {items, error}
}) => ({
  items,
  error,
  undoneCount: items.filter(item => !item.done).length,
})

const mapDispatchToProps = {
  fetchTodos,
  setTodoStatus,
  deleteTodo
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoList)
