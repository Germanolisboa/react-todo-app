import React, {useEffect, useRef, useContext} from 'react'
import styled from 'styled-components'
import TodoList from './TodoList'
import scrollerContext from './utils/ScrollContext'

const StyledContent = styled.div`
  position: absolute;
  top: 50px;
  left: 0;
  height: calc(100% - 50px);
  width: 100%;
  padding-top: 40vh;
  overflow-y: auto;
`

const TodoContent = () => {
  const {setScroller} = useContext(scrollerContext)
  const scrollerRef = useRef();

  useEffect(() => {
    setScroller(scrollerRef)
  }, [scrollerRef, setScroller])

  return(
    <StyledContent ref={scrollerRef}>
      <TodoList />
    </StyledContent>
  )
}

export default TodoContent
