import {useEffect, useState} from 'react';

export const useScrollerPosition = scrollerRef => {
  const [scrollPos, setPos] = useState({isSet: false, value: 150})
  function setPosValue() {
    setPos({
      isSet: true,
      value: scrollerRef.current.children[0].getBoundingClientRect().y
    })
  }
  useEffect(() => {
    if (scrollerRef && scrollerRef.current) {
      scrollerRef.current.addEventListener('scroll', setPosValue)

      return () => {
        scrollerRef.current.removeEventListener('scroll', setPosValue)
      }
    }
  }, [scrollerRef])

  return scrollPos
}
