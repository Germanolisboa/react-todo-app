import React from 'react'

const scrollerContext = React.createContext({
  scrollerRef: {},
  setScrollerRef: () => {}
})

export default scrollerContext
