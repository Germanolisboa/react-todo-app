import React, {useEffect, useRef, useState} from 'react'
import styled from 'styled-components'

const StyledShade = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,.7);
  opacity: 0;
  pointer-events: none;
  z-index: 5;
  transition: opacity 300ms linear 0s;

  &.visible {
    opacity: 1;
    pointer-events: all; 
  }
`
const StyledDiv = styled.div`
  --top: 50px;
  --translate: calc(-100% - var(--top));
  --opacity: 0;
  position: absolute;
  left: 10%;
  top: var(--top);
  width: 80%;
  height: 200px;
  background-color: white;
  transform: translateY(var(--translate));
  opacity: var(--opacity);
  transition: transform 400ms ease 0s, opacity 300ms ease 0s;
  will-change: opacity, transform;

  &.visible {
    --translate: 0;
    --opacity: 1;
  }

  input {
    /* display: none; */
  }
  .warning {
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`

const EditTodo = ({match, items, ...props}) => {
  const input = useRef()
  const isNew = match && match.id === 'new'

  const [currentTodo, setCurrent] = useState(null)

  useEffect(() => {
    if (match) {
      input.current.focus()
    } else {
      input.current.blur()
    }

  }, [match])

  const goBack = e => {
    if (e.target === e.currentTarget) {
      props.history.go(-1)
    }
  }

  return (
    <StyledShade
      onClick={goBack}
      className={match && 'visible'}
    >
      <StyledDiv className={match ? 'visible' : ''}>

        {currentTodo &&
          <div>todo {currentTodo.text}</div>
        }

        <div>
          <label>Data para terminar:</label>
          <input type="date"/>
        </div>
        <div>
          <label>Detalhes</label>
          <input ref={input} type="text" />
        </div>
      </StyledDiv> 
    </StyledShade>
  )
}

export default EditTodo
