import React, {useState} from 'react'
import styled from 'styled-components'
import TodoTitle from './TodoTitle'
import TodoContent from './TodoContent'
import scrollerContext from './utils/ScrollContext'
import {getStore} from "../redux"
import {Provider} from "react-redux";
import {BrowserRouter} from 'react-router-dom'
import {Link} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const StyledDiv = styled.div`
  height: 100vh;
  width: 100vw;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  position: relative;
`
const FabLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 20px;
  right: 20px;
  height: 60px;
  width: 60px;
  border-radius: 50%;
  background-color: var(--accent-color);
  z-index: 5;
  box-shadow: 0 5px 12px -4px rgba(0,0,0,.5);
  color: white;
  cursor: pointer;
`

const TodoApp = () => {
  const [scrollerRef, setScroller] = useState()
  const [store] = useState(getStore())

  function backToTop() {
    scrollerRef &&
      scrollerRef.current.scrollBy({
        top: -window.innerHeight,
        behavior: 'smooth'
      })
  }

  return (
    <BrowserRouter>
      <scrollerContext.Provider value={{setScroller}}> {/* context provider for title animation */}
        <Provider store={store}> {/* redux store Provider */}

          <StyledDiv className="main-wrapper"> {/* app UI */}
            <TodoTitle
              onClick={backToTop}
              scrollerRef={scrollerRef}
            />
            <TodoContent />

            <FabLink to="/new">
              <FontAwesomeIcon icon="plus" />
            </FabLink>
            
          </StyledDiv>
        </Provider>
      </scrollerContext.Provider>
    </BrowserRouter>
  )
}

export default TodoApp
