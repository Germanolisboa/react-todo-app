import API from './api'

const client = {
  fetchTodos: () => API.get('/todo'),
  fetchTodo: id => API.get(`/todo/${id}`),
  updateTodo: (id, params) => API.put(`/todo/${id}`, {...params}),
  delete: id => API.delete(`/todo/${id}`),
  createTodo: params => API.post(`/todo`, {params})
}

export {client}
