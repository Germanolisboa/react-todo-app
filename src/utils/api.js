import axios from 'axios';

export default axios.create({
  baseURL: `https://5c9eca27595c55001487bfa0.mockapi.io/`
});
