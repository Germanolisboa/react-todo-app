import {createActions, handleActions, combineActions} from 'redux-actions'
import {client} from '../../utils/client'

const {
  startFetchTodos,
  fetchTodosSuccess,
  fetchTodosError,
  startCreateTodo,
  createTodoSuccess,
  startUpdateTodo,
  updateTodoSuccess,
  removeOne
} = createActions(
  'START_FETCH_TODOS',
  'FETCH_TODOS_SUCCESS',
  'FETCH_TODOS_ERROR',
  'START_CREATE_TODO',
  'CREATE_TODO_SUCCESS',
  'START_UPDATE_TODO',
  'UPDATE_TODO_SUCCESS',
  'REMOVE_ONE'
)
  
const initialState = {
  isLoading: false,
  items: [],
  error: null,
  isCreating: false
}
  
export default handleActions({
  START_FETCH_TODOS: (state) => ({...state, isLoading: true}),
  START_CREATE_TODOS: (state) => ({...state, isCreating: true}),

  CREATE_TODOS_SUCCESS: (state, payload) => ({
    ...state,
    items: {
      ...state.items,
      ...payload
    },
    isCreating: false
  }),

  START_UPDATE_TODO: (state) => ({...state, isLoading: true}),
  UPDATE_TODO_SUCCESS: (state, {payload}) => {
    const items = state.items.map(item => {
      return item.id === payload.id ? payload : item
    })
    return {
      ...state,
      items
    }
  },
  
  [combineActions(
    fetchTodosSuccess,
    fetchTodosError
  )]: (
    state,
    {error, payload}
  ) => ({
    ...state,
    isLoading: false,
    items: !error ? payload : null,
    error: error ? payload : null
  }),

  REMOVE_ONE: (state, {payload}) => ({
    ...state,
    items: state.items.filter(item => item.id !== payload)
  })
}, initialState)

export const fetchTodos = () => dispatch => {
  dispatch(startFetchTodos)

  client.fetchTodos()
    .then(todos => fetchTodosSuccess(todos.data))
    .catch(err => console.log(err))
    .then(dispatch)
}

export const createTodo = todo => dispatch => {
  dispatch(startCreateTodo)

  client.createTodo(todo)
    .then(todo => createTodoSuccess(todo.data))
    .catch(err => console.log(err))
    .then(dispatch)
}

export const setTodoStatus = params => dispatch => {
  const {id, ...rest} = params
  dispatch(startUpdateTodo)

  client.updateTodo(id, {...rest})
    .then(() => updateTodoSuccess(params))
    .catch(err => console.log(err))
    .then(dispatch)
}

export const deleteTodo = id => dispatch => {
  client.delete(id)
    .then(() => removeOne(id))
    .catch(err => console.log(err))
    .then(dispatch)
}
      
