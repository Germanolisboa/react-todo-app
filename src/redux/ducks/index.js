import {combineReducers} from "redux";
import todos from "./todos";

export default combineReducers({
    todos
});

export const getTodo = (id, items) => items.find(item => item.id === id)
