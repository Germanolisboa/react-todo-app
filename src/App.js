import React, {useEffect} from 'react'
import TodoApp from './components/TodoApp'

import {library} from '@fortawesome/fontawesome-svg-core'
import {faTrashAlt, faCheck, faCalendarWeek, faPlus, faPen, faEllipsisV} from '@fortawesome/free-solid-svg-icons'

library.add(faTrashAlt, faCheck, faCalendarWeek, faPlus, faPen, faEllipsisV)

const App = () => {
  useEffect(() => {
    const color = getComputedStyle(document.body).getPropertyValue('--bg-color-1')

    const metaThemeColor = document.querySelector("meta[name=theme-color]");
    metaThemeColor.setAttribute("content", color);
  })

  return (
    <div className="App">
      <TodoApp />
    </div>
  );
}


export default App;
